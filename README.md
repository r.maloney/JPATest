JPATest
=======
This project is a minimal example using Guice and JPA with Hibernate as a provider and gradle as a build system. The intention is to demonstrate the use of a custom JPA persistence provider wrapping the existing one. This enables JDBC connection information, most notably passwords, to be retrieved instead of embedded in the persistence.xml file. It uses an in memory database (H2) for convenience

To build this project, run 

```
gradlew
```
on *nix or 

```
gradlew.bat
```

on Windows. This will download the latest gradle and build the project. The two most interesting tasks are 

```
gradle run
```

and 

```
gradle test
```

which will run a sample application and the unit tests respectively. Both create an object using a Guice injector then and use hibernate (via Guice-JPA) to persist it to the database. The results of the test can be found in {PROJECT_HOME}/build/reports/test/index.html
