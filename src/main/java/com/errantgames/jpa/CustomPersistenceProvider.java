package com.errantgames.jpa;

import org.hibernate.jpa.AvailableSettings;
import org.hibernate.jpa.HibernatePersistenceProvider;

import javax.persistence.EntityManagerFactory;
import javax.persistence.spi.PersistenceProvider;
import javax.persistence.spi.PersistenceUnitInfo;
import javax.persistence.spi.ProviderUtil;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by ryan on 3/29/14.
 */
public class CustomPersistenceProvider implements PersistenceProvider {

    private final HibernatePersistenceProvider hibernatePersistenceProvider;

    public CustomPersistenceProvider() {
        this.hibernatePersistenceProvider = new HibernatePersistenceProvider();
    }


    @Override
    public EntityManagerFactory createEntityManagerFactory(String emName, Map map) {
        //Retrieve and inject credentials into properties map here.
        map = new HashMap();
        //Hibernate checks to be sure that the provider specified in persistence.xml is one of the two
        //it provides. We need to pretend as though that is the case for the sake of our wrapper.
        map.put(AvailableSettings.PROVIDER, "org.hibernate.jpa.HibernatePersistenceProvider");
        return hibernatePersistenceProvider.createEntityManagerFactory(emName, map);
    }

    @Override
    public EntityManagerFactory createContainerEntityManagerFactory(PersistenceUnitInfo info, Map map) {
        //Retrieve and inject credentials into properties map here.
        return hibernatePersistenceProvider.createContainerEntityManagerFactory(info, map);
    }

    @Override
    public void generateSchema(PersistenceUnitInfo info, Map map) {
        //Retrieve and inject credentials into properties map here.
        hibernatePersistenceProvider.generateSchema(info, map);
    }

    @Override
    public boolean generateSchema(String persistenceUnitName, Map map) {
        //Retrieve and inject credentials into properties map here.
        return hibernatePersistenceProvider.generateSchema(persistenceUnitName, map);
    }

    @Override
    public ProviderUtil getProviderUtil() {
        return hibernatePersistenceProvider.getProviderUtil();
    }
}
