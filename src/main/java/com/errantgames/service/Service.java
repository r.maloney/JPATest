package com.errantgames.service;

import com.errantgames.entity.Widget;

/**
 * Created by ryan on 4/10/14.
 */
public interface Service {
    void process(Widget widget);
    Widget findWidget(long id);
}
