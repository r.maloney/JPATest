package com.errantgames.service;

import com.errantgames.entity.Widget;
import com.errantgames.guice.ApplicationInitializer;
import com.errantgames.guice.ProductionModule;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.persist.Transactional;
import com.google.inject.persist.jpa.JpaPersistModule;

import javax.persistence.EntityManager;

/**
 * Created by ryan on 4/10/14.
 */
public class StandardService implements Service{
    @Inject
    private EntityManager entityManager;

    @Transactional
    public void process(Widget widget) {
        if (null == entityManager.find(Widget.class, widget.getId())) {
            entityManager.persist(widget);
        }
    }

    @Transactional
    public Widget findWidget(long id) {
        return entityManager.find(Widget.class, id);
    }

    public static void main(String... args) {
        Injector injector = Guice.createInjector(new JpaPersistModule("jpa-persist-unit-test"), new ProductionModule());
        ApplicationInitializer applicationInitializer = injector.getInstance(ApplicationInitializer.class);

        Service service = injector.getInstance(Service.class);

        Widget widget = new Widget(7, "ProdTestWidget");

        service.process(widget);

        Widget fetchedWidget = service.findWidget(widget.getId());

        System.out.println("-----------------------------------------");
        System.out.println("TEST COMPLETE, Fetched widget name is: " + fetchedWidget.getName());
        System.out.println("-----------------------------------------");

        applicationInitializer.stop();
    }
}
