package com.errantgames.guice;

import com.google.inject.Inject;
import com.google.inject.persist.PersistService;

/**
 * Created by ryan on 4/10/14.
 */
public class ApplicationInitializer {
    private PersistService persistService;

    @Inject
    public ApplicationInitializer(PersistService persistService) {
        this.persistService = persistService;
        persistService.start();
    }

    public void stop() {
        persistService.stop();
    }
}
