package com.errantgames.guice;

import com.errantgames.service.Service;
import com.errantgames.service.StandardService;
import com.google.inject.AbstractModule;

/**
 * Created by ryan on 3/25/14.
 */
public class ProductionModule extends AbstractModule {
    @Override
    protected void configure() {
        bind(Service.class).to(StandardService.class);
    }
}
