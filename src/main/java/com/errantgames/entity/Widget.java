package com.errantgames.entity;

import javax.persistence.*;

/**
 * Created by ryan on 3/25/14.
 */
@Entity
@Table(name="WIDGET")
public class Widget {

    @Id
    @Column(name="WIDGET_ID")
    private long id;
    private String name;

    public Widget(long id, String name) {
        this.id = id;
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
