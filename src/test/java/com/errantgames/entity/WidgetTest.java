package com.errantgames.entity;

import com.errantgames.guice.ApplicationInitializer;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.persist.jpa.JpaPersistModule;
import junit.framework.TestCase;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.persistence.EntityManager;

/**
 * Created by ryan on 3/29/14.
 */
public class WidgetTest extends TestCase{
    private Injector injector;
    private EntityManager entityManager;
    private ApplicationInitializer applicationInitializer;

    @Before
    public void setUp() {
        injector = Guice.createInjector(new JpaPersistModule("jpa-persist-unit-test"));
        applicationInitializer = injector.getInstance(ApplicationInitializer.class);
        entityManager = injector.getInstance(EntityManager.class);
    }

    @After
    public void tearDown() {
        applicationInitializer.stop();
    }

    @Test
    public void testCreate() {
        entityManager.getTransaction().begin();

        assertNull(entityManager.find(Widget.class, 1L));

        Widget widget = new Widget(1, "TestWidget");

        entityManager.persist(widget);

        Widget foundWidget = entityManager.find(Widget.class, 1L);

        assertNotNull(foundWidget);
        assertEquals(1, foundWidget.getId());
        assertEquals("TestWidget", foundWidget.getName());

        entityManager.getTransaction().commit();
    }
}
